export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'AppointmentFrontend',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'ant-design-vue/dist/antd.css',
    '@/assets/css/main.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/antd-ui'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth'
  ],
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/users/login', method: 'post', propertyName: 'token' },
          user: { url: '/users/getlog', method: 'get', propertyName: false },
          logout: { url: 'users/logout', method: 'post' }
        }
      },
    }
  },
  
  // auth: {
  
  //   strategies: {
  //     localStud: {
  //       scheme: 'local',
  //       endpoints: {
  //         login: { url: '/students/login', method: 'post', propertyName: 'token' },
  //         user: { url: '/students/', method: 'get', propertyName: 'user' },
  //         logout: { url: 'users/logout', method: 'post' }
  //       }
  //     },
  //     localProf: {
  //       scheme: 'local',
  //       endpoints: {
  //         login: { url: 'professors/login', method: 'post', propertyName: 'token' },
  //         user: { url: 'professors/user', method: 'get', propertyName: 'user' },
  //         logout: { url: 'users/logout', method: 'post' }
  //       }
  //     }
  //   }
  // },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: 'http://localhost:4000',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
