const Sequelize = require("sequelize");
// Connect to database
const sequelize = new Sequelize({
  hostname: "localhost",
  database: "appointment",
  username: "root",
  password: null,
  dialect: "mysql",
});

module.exports = sequelize;