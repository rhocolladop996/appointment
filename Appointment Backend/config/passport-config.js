const bcrypt = require('bcrypt');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;


//Authenticate Professor or Student
const ProfessorModel = require('../models/professor');
const StudentModel = require('../models/student');

module.exports = (passport) => {
  passport.use('local-professor', new LocalStrategy({
      usernameField : 'email', // use email as the username field
      passwordField : 'password', // use password as the password field
      passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    // find the user by their email
    async (req, email, password, done) => { 
      let user = await ProfessorModel.findOne({ where: { email: email } }); 
    // if email not found in ProfessorModel, search in StudentModel
      if (!user) {
        user = await StudentModel.findOne({ where: { email: email } }); 
        if (!user) {
          done(null, false, { message: 'Invalid email' });
          return;
        }
      }
      
      // Validate Password
      const validPassword = await bcrypt.compare(password, user.password);

      if (!validPassword) {
         done(null, false, { message: 'Invalid password'});
         return;
      }
      
      done(null, user);
    })
  );
};



passport.serializeUser((user, done) => {
    done(null, user.id);
  });
  
passport.deserializeUser(async function(id, done) {
    const user = await UserModel.findByPk(id);
    done(null, user);
});