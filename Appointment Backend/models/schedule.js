const Sequelize = require("sequelize");
const sequelize = require("../utils/database");

// Create Schedule Model
const Schedule = sequelize.define("schedule",{
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    date:{
        type: Sequelize.STRING,
    },
    startTime:{
        type: Sequelize.STRING, 
    },
    endTime:{
        type: Sequelize.STRING, 
    },
});

//Association related to schedule model
const Professor = require("./professor");
const Student = require("./student")

Schedule.belongsTo(Professor, { foreignKey: 'professor_id' });
Schedule.belongsTo(Student, { foreignKey: 'student_id' });

module.exports = Schedule;