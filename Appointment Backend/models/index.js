const sequelize = require("../utils/database");


//Models for Appointment System
const ProfessorModel = require("./professor")
const ScheduleModel = require("./schedule")
const StudentModel = require("./student")

// Create Table when not yet existing
ProfessorModel.sync();
ScheduleModel.sync();
StudentModel.sync();

module.exports = sequelize;