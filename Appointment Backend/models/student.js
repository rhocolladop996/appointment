const Sequelize = require("sequelize");
const sequelize = require("../utils/database");

// Create Student Model
const Student = sequelize.define("student",{
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    firstname:{
        type: Sequelize.STRING,
    },
    lastname:{
        type: Sequelize.STRING, 
    },
    email:{
        type: Sequelize.STRING,
    },
    password:{
        type: Sequelize.STRING,
    },
    role:{
        type: Sequelize.STRING,
    },
    isActive:{
        type: Sequelize.BOOLEAN,
        defaultValue: true
    }
});

module.exports = Student;