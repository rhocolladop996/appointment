const express = require("express");
const createError = require("http-errors");
const UserRouter = require("./routes/users");
const bodyParser = require("body-parser");
const app = express();
const cors = require('cors')
const passport = require('passport');
const passportConfig = require('./config/passport-config');
const session = require('express-session');

app.use(cors())

app.use(bodyParser.json());


//Session
app.use(session({
  secret: process.env.SECRET_KEY,
  resave: false,
  saveUninitialized: false,
  cookie: { secure: true }
}));

//passport
passportConfig(passport);
app.use(passport.initialize());
app.use(passport.session());


app.use("/users", UserRouter)

//appointment
const ProfessorRouter = require('./routes/professors')
const StudentRouter = require('./routes/student')
const ScheduleRouter = require('./routes/schedules')

app.use("/professors", ProfessorRouter)
app.use("/students", StudentRouter)
app.use("/schedules", ScheduleRouter)

app.use((req, res) => {
  res.send(createError(404));
});
module.exports = app;