const express = require("express");
const UserController = require("../controllers/user")
const router = express.Router();
const authMiddleware = require('../middleware/authMiddelware');


router.post("/login",UserController.login)
router.get('/getlog',authMiddleware, UserController.getUser)
router.post('/logout', UserController.logout)
router.put('/change/password', authMiddleware, UserController.changePassword)
router.post('/forgotPassword',UserController.forgotPassword)
router.post('/checkPassword',authMiddleware,UserController.checkPassword)


module.exports = router;
