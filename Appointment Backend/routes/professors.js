const express = require("express");
const ProfessorController = require("../controllers/professor")
const router = express.Router();
const authMiddleware = require('../middleware/authMiddelware');

router.put('/:id',authMiddleware,ProfessorController.updateProfile)
router.get('/id/:id',authMiddleware, ProfessorController.getID)
router.get('/',authMiddleware, ProfessorController.getProfessors)
router.post('/', ProfessorController.createProfessor)



module.exports = router;