const express = require("express");
const StudentController = require("../controllers/student")
const router = express.Router();
const authMiddleware = require('../middleware/authMiddelware');

router.put('/:id',authMiddleware,StudentController.updateProfile)
router.get('/',authMiddleware, StudentController.getStudents)
router.post('/', StudentController.createStudent)

module.exports = router;