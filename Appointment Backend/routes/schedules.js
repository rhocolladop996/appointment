const express = require("express");
const ScheduleController = require("../controllers/schedule")
const router = express.Router();
const authMiddleware = require('../middleware/authMiddelware');

router.put('/update/:id',authMiddleware, ScheduleController.updateSchedule)
router.put('/:id',authMiddleware, ScheduleController.bookSchedule)
router.post('/:id',authMiddleware, ScheduleController.createSchedule)
router.get('/:id',authMiddleware,ScheduleController.getProfStud)
router.get('/',authMiddleware, ScheduleController.getSchedules)
router.delete("/:id",authMiddleware,ScheduleController.deleteSchedule)

module.exports = router;