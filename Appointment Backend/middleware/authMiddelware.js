// Middleware for Authorization and protecting routes
const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  // Check if the request contains a token in the Authorization header
  const authHeader = req.headers.authorization;
  if (!authHeader) {
    return res.status(401).json({ message: 'Authorization header missing' });
  }

  // Extract the token from the Authorization header
  const token = authHeader.split(' ')[1];

  // Verify the token
  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.userId = decoded.id;
    req.userRole = decoded.role
    next();
  } catch (err) {
    return res.status(401).json({ message: 'Invalid token' });
  }
};
