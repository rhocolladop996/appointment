const ScheduleModel = require('../models/schedule')
const StudentModel = require('../models/student')
const ProfessorModel = require('../models/professor')

//Create a Schedule
exports.createSchedule = async (req, res) => {
    try {
      const { body } = req;
      const professorId = req.params.id;
  
      const newSched = await ScheduleModel.create({
        date: body.date,
        startTime: body.startTime,
        endTime: body.endTime,
        professor_id: professorId,
      });
  
      res.send(newSched);
    } catch (error) {
      console.log(error);
      res.status(500).send("An error occurred while creating the schedule.");
    }
  };

//update a Schedule
  exports.updateSchedule = async (req, res) => {
    let { id } = req.params;
    let { startTime, endTime} = req.body;
    let schedule = await ScheduleModel.findByPk(id);
    schedule.update({ startTime, endTime });
    res.send(schedule);
};
//Book a schedule by filling the student id with a value
  exports.bookSchedule = async (req, res) => {
    try {
      const { student_id } = req.body;
      const scheduleId = req.params.id;
  
      const schedule = await ScheduleModel.findByPk(scheduleId);
  
      if (!schedule) {
        return res.status(404).json({ message: "Schedule not found" });
      }
  
      schedule.student_id = student_id;
      await schedule.save();
  
      res.status(200).json(schedule);
    } catch (err) {
      console.error(err);
      res.status(500).json({ message: "Server error" });
    }
  };
  
  //Get Professor's created Schededules
  exports.getProfSchedule = async (req, res) => {
    try {
      const { id } = req.params;
      const schedules = await ScheduleModel.findAll({
        where: { professor_id: id },
      });
      res.status(200).json(schedules);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Internal server error' });
    }
  };

  //get all schedules
  exports.getSchedules = async (req,res) =>{
    let schedules = await ScheduleModel.findAll();
    res.send(schedules);
};

//get professor's schedules with names of student
exports.getProfStud = async (req, res) => {
  
  try {
    const schedules = await ScheduleModel.findAll({
      include: [{
        model: ProfessorModel,
        where: { id: req.params.id }
      }, {
        model: StudentModel,
        attributes: ['firstname']
      }]
    });

    res.send(schedules);
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal server error');
  }
};


//DELETE Schedule BY ID
exports.deleteSchedule = async (req, res) => {
  let { id } = req.params;
  let schedule = await ScheduleModel.findByPk(id);
  if (!schedule){
      return res.status(404).json({ message: 'schedule not found' });
  }
  await schedule.destroy();
  res.send(`Successfully deleted schedule with id ${id}`);
};
  