const StudentModel = require('../models/student')
const ProfessorModel = require('../models/professor')
const bcrypt = require('bcrypt');

//create a student
exports.createStudent = async (req, res) => {
  const { body } = req;
   // Check if email already exists in StudentModel
  const existingStudent = await StudentModel.findOne({ where: { email: body.email } });
  if (existingStudent) {
    return res.status(409).json({ error: 'Email already exists' });
  }
   // Check if email already exists in Professor Model
  const existingProfessor = await ProfessorModel.findOne({ where: { email: body.email } });
  if (existingProfessor) {
    return res.status(409).json({ error: 'Email already exists' });
  }
  // Create the Student if email does not exist
  const hashedPassword = await bcrypt.hash(body.password, 10);
  const user = await StudentModel.create({
    ...body,
    password: hashedPassword
  });
  
  res.send(user);
};

//get all students
exports.getStudents = async (req,res) =>{
  let students = await StudentModel.findAll();
  res.send(students);
};


// Update student
exports.updateProfile = async (req, res) => {
  let { id } = req.params;
  let { firstname, lastname, email, password } = req.body;
  let student = await StudentModel.findByPk(id);

  // Update the fields that were passed in the request body
  student.firstname = firstname;
  student.lastname = lastname;
  student.email = email;

  // If a password was provided, hash it and update the student's password
  if (password) {
    const saltRounds = 10;
    const hashedPassword = await bcrypt.hash(password, saltRounds);
    student.password = hashedPassword;
  }

  await student.save();

  res.send(student);
};