const ProfessorModel = require('../models/professor')
const bcrypt = require('bcrypt');
const StudentModel = require('../models/student')


// create professor
exports.createProfessor = async (req, res) => {
  const { body } = req;
  
  // Check if email already exists in ProfessorModel
  const existingProfessor = await ProfessorModel.findOne({ where: { email: body.email } });
  if (existingProfessor) {
    return res.status(409).json({ error: 'Email already exists' });
  }
  
  // Check if email already exists in StudentModel
  const existingStudent = await StudentModel.findOne({ where: { email: body.email } });
  if (existingStudent) {
    return res.status(409).json({ error: 'Email already exists' });
  }
  // Create the Professor if email does not exist
  const hashedPassword = await bcrypt.hash(body.password, 10);
  const user = await ProfessorModel.create({
    ...body,
    password: hashedPassword
  });
  
  res.send(user);
};


//get Professors
exports.getProfessors = async (req,res) =>{
    let professors = await ProfessorModel.findAll();
    res.send(professors);
};


// get professor by ID
exports.getID = async (req, res) => {
  let { id } = req.params;
  let professor = await ProfessorModel.findByPk(id);
  res.send(professor);
};


// Update Professor Profile
exports.updateProfile = async (req, res) => {
  let { id } = req.params;
  let { firstname, lastname, email, password } = req.body;
  let professor = await ProfessorModel.findByPk(id);

  // Update the fields that were passed in the request body
  professor.firstname = firstname;
  professor.lastname = lastname;
  professor.email = email;

  // If a password was provided, hash it and update the professor's password
  if (password) {
    const saltRounds = 10;
    const hashedPassword = await bcrypt.hash(password, saltRounds);
    professor.password = hashedPassword;
  }

  await professor.save();

  res.send(professor);
};




