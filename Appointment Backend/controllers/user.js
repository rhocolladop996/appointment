// Controller for all users

const passport = require('passport');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const StudentModel = require('../models/student')
const ProfessorModel = require('../models/professor')
const crypto = require('crypto');
const bcrypt = require('bcrypt')

//logout
exports.logout = async (req, res) => {
    try {
      req.session.destroy((err) => {
        if (err) console.log(err);
        res.clearCookie("session");
        res.status(200).json({ message: 'Logout successful.' });
      });
    } catch (error) {
      console.log(error);
      res.status(500).send({ message: "Something went wrong" });
    }
};

//Forgot Password
exports.forgotPassword = async (req, res) => {
  const { email } = req.body;
  let user = await ProfessorModel.findOne({ where: { email: email } }); // find the user by their email
      
  if (!user) {
    user = await StudentModel.findOne({ where: { email: email } }); // if email not found in ProfessorModel, search in StudentModel
    if (!user) {
      
      return res.status(404).json({ message: 'User not found.' });
    }
  }

  // Generate new password using random characters and hash it
  const newPassword = crypto.randomBytes(4).toString('hex');
  const hashedPassword = await bcrypt.hash(newPassword, 10);
  // Update user's password
  user.password = hashedPassword;
  await user.save();

  // Send email with the newly created password to the user
  const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: process.env.GMAIL_EMAIL,
      pass: process.env.GMAIL_PASSWORD,
    },
  });

  const mailOptions = {
    from: process.env.EMAIL_USER,
    to: email,
    subject: 'Password reset',
    text: `Your new password is: ${newPassword}`,
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
      res.status(500).json({ message: 'Failed to send email.' });
    } else {
      console.log('Email sent: ' + info.response);
      res.json({ message: 'Email sent.' });
    }
  });
}; 

//Get Logged in User
exports.getUser= async (req, res) => {
  const userId = req.userId;
  const userRole = req.userRole;
  let user; 
  if(userRole == "professor"){
    user = await ProfessorModel.findByPk(userId)
  }else{
    user = await StudentModel.findByPk(userId)
  }
  let firstname = user.firstname
  let lastname = user.lastname
  let email = user.email
  let role = user.role
  let id = user.id
  res.send({email, firstname, lastname, role, id});
};

//login 
exports.login = async (req, res, next) => {
    passport.authenticate('local-professor', async (err, user, info) => {
      if (err) {
        next(err);
      }
  
      if (!user) {
         res.status(401).json({ message: 'Invalid email or password' });
      }
  
      try {
        // Log the user in
        await req.login(user, { session: false });
        const token = jwt.sign({ id: user.id, role:user.role }, process.env.JWT_SECRET);
        res.status(200).json({ message: 'Logged in successfully', token: token });
      } catch (error) {
        next(error);
      }
    })(req, res, next);
  };

  //Password Validator if the same
  exports.checkPassword = async (req, res) => {
  const userId = req.userId;
  const userRole = req.userRole;
  const password = req.body.password;
  let user;
  if(userRole == "professor"){
    user = await ProfessorModel.findByPk(userId)
  }else{
    user = await StudentModel.findByPk(userId)
  }
  // get the user's password hash from the database
  const hash = user.password;
  
  // compare the provided password with the hash
  const isMatch = await bcrypt.compare(password, hash);
  
  if (isMatch) {
    res.status(200).send({ message: 'Password is correct' });
  } else {
    res.status(401).send({ message: 'Incorrect password' });
  }
};

//change password
exports.changePassword = async (req, res) => {
const userId = req.userId;
const userRole = req.userRole;
const newPassword = req.body.newPassword;
let user;
// Find user depending on role
if(userRole == "professor"){
  user = await ProfessorModel.findByPk(userId)
}else{
  user = await StudentModel.findByPk(userId)
}
// hash the new password
const hashedPassword = await bcrypt.hash(newPassword, 10);
// change password then save
user.password = hashedPassword;
await user.save();

res.send("Password updated successfully.");
};