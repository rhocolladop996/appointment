require('dotenv').config();
const http = require("http");
const app = require("./app.js");
const server = http.createServer(app);
const models = require("./models");

// //Session
// const session = require('express-session');
// app.use(session({
//   secret: 'secret-key',
//   resave: false,
//   saveUninitialized: false,
//   cookie: { secure: true }
// }));
// //passport
// const passport = require('passport');
// const passportConfig = require('./config/passport-config');

// passportConfig(passport);
// app.use(passport.initialize());
// app.use(passport.session());


models.sync().then(() => {
  const port = process.env.PORT || 4000;
  server.listen(port, () =>
    console.log("Server is running at http://localhost:" + port)
  );
});